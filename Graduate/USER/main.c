#include "stdio.h"
#include "string.h"
#include "delay.h"
#include "usart1.h"
#include "usart2.h"
#include "esp8266.h"
#include "led.h"
#include "stm32f10x.h"

#define DATA_LEN	16  			//定义最大接收字节数 16

#define RAIN_MAX	40				//雨滴最大监测值，超出关窗
#define LIGHT_MAX	50				//超出阈值，开灯
#define WENDU_MAX   28				//超出阈值，开空调
#define MQ2_MAX		50				//超出阈值，警报

u8 ZigBee_data_recive[DATA_LEN];	//存放ZigBee发送过来的数据
u8 ZigBee_data_send[DATA_LEN];		//存放将发送至ZigBee的数据


u8 motor_flag=0;					//节点二的步进电机标志（窗户），一开始关闭
u8 relay2_flag=0;					//节点二的继电器标志（空调），一开始关闭
u8 buzzer_flag=0;					//协调器的蜂鸣器标志（警报器），一开始关闭
u8 led_flag=0;						//协调器的LED标志（灯），一开始关闭

void ZigBee_event(u8 data[]);		//本地处理ZigBee的任务
void device_state(u8 data[]);		//设备状态监控

int main(void)
{  
	u16 len;	
	delay_init();			//延迟函数初始化
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2); //设置NVIC中断分组2:2位抢占优先级，2位响应优先级
	Uart1_Init(115200);		//串口一初始化
	Uart2_Init(115200);		//串口二初始化
	Led_Init();				//LED0/LED1初始化
	Connect_TCP();			//连接tcp服务器
	
	
	delay_ms(10);
	while(1)
	{
		if(USART_RX_STA&0x8000)		//接收完成,USART_RX_STA==0x8000
		{					   
			len=USART_RX_STA&0x3fff;	//得到此次接收到的数据长度
			memset(ZigBee_data_recive,0,16);	//清0
			memcpy(ZigBee_data_recive,USART_RX_BUF,len);	//复制到ZigBee_data
			
			if(auto_flag==2)		//是否自主控制
			{
				ZigBee_event(ZigBee_data_recive);
			}
			device_state(ZigBee_data_recive);		//获取自主控制设备的状态
			Uart2_SendString(ZigBee_data_recive);	//通过wifi模块发送出去
			USART_RX_STA=0;
		}
		
		LED0 = !LED0;	//闪烁
		delay_ms(10);
	}
	return 0;
}

void ZigBee_event(u8 data[])
{
	if(data[0]==0xa5&&data[1]==0x5a)	//帧头判断
	{
		if(data[3] == 0x02)				//数据？
		{
			switch (data[4])
				{
				case 0x01:	//雨滴传感器，控制窗户，步进电机
					if(data[5]>=RAIN_MAX&&motor_flag==1)		//雨水高 且 窗户开着，则可以关
					{
						memset(ZigBee_data_send,0,16);
						ZigBee_data_send[0] = 0xa5;
						ZigBee_data_send[1] = 0x5a;
						ZigBee_data_send[2] = 0x02;		//节点2
						ZigBee_data_send[3] = 0x01;		//控制
						ZigBee_data_send[4] = 0x03;		//步进电机
						ZigBee_data_send[5] = 0x01;		//正转，关闭
						ZigBee_data_send[6] = 0x02;		//两圈
						ZigBee_data_send[7] = 0xff;
						ZigBee_data_send[8] = 0xff;
						ZigBee_data_send[9] = 0x0a;		//长度
						ZigBee_data_send[10] = 0xff;	//校验位
						ZigBee_data_send[11] = 0x0d;
						ZigBee_data_send[12] = 0x0a;
						printf("%s",ZigBee_data_send);	//发送控制命令
						motor_flag=0;					//关闭状态
					}
					else if(data[5]<=RAIN_MAX&&motor_flag==0)	//雨水值低 且 窗户关着，则可以开
					{
						memset(ZigBee_data_send,0,16);
						ZigBee_data_send[0] = 0xa5;
						ZigBee_data_send[1] = 0x5a;
						ZigBee_data_send[2] = 0x02;		//节点2
						ZigBee_data_send[3] = 0x01;		//控制
						ZigBee_data_send[4] = 0x03;		//步进电机
						ZigBee_data_send[5] = 0x02;		//反转，打开
						ZigBee_data_send[6] = 0x02;		//两圈
						ZigBee_data_send[7] = 0xff;
						ZigBee_data_send[8] = 0xff;
						ZigBee_data_send[9] = 0x0a;		//长度
						ZigBee_data_send[10] = 0xff;	//校验位
						ZigBee_data_send[11] = 0x0d;
						ZigBee_data_send[12] = 0x0a;
						printf("%s",ZigBee_data_send);	//发送控制命令
						motor_flag=1;					//打开状态
					}					
					break;
					
				case 0x02:	//温湿度传感器,控制节点2上的继电器打开,相当于空调
					if(data[5]>=WENDU_MAX&&relay2_flag==0)		//超出阈值,且空调是关闭的，开
					{
						memset(ZigBee_data_send,0,16);
						ZigBee_data_send[0] = 0xa5;
						ZigBee_data_send[1] = 0x5a;
						ZigBee_data_send[2] = 0x02;		//节点2
						ZigBee_data_send[3] = 0x01;		//控制
						ZigBee_data_send[4] = 0x01;		//继电器
						ZigBee_data_send[5] = 0x01;		//开
						ZigBee_data_send[6] = 0xff;		
						ZigBee_data_send[7] = 0xff;
						ZigBee_data_send[8] = 0xff;
						ZigBee_data_send[9] = 0x0a;		//长度
						ZigBee_data_send[10] = 0xff;	//校验位
						ZigBee_data_send[11] = 0x0d;
						ZigBee_data_send[12] = 0x0a;
						printf("%s",ZigBee_data_send);	//发送控制命令
						relay2_flag=1;					//打开状态
					}
					else if(data[5]<=WENDU_MAX&&relay2_flag==1)	//低于阈值，且空调是打开的，关
					{
						memset(ZigBee_data_send,0,16);
						ZigBee_data_send[0] = 0xa5;
						ZigBee_data_send[1] = 0x5a;
						ZigBee_data_send[2] = 0x02;		//节点2
						ZigBee_data_send[3] = 0x01;		//控制
						ZigBee_data_send[4] = 0x03;		//继电器
						ZigBee_data_send[5] = 0x02;		//关
						ZigBee_data_send[6] = 0xff;		
						ZigBee_data_send[7] = 0xff;
						ZigBee_data_send[8] = 0xff;
						ZigBee_data_send[9] = 0x0a;		//长度
						ZigBee_data_send[10] = 0xff;	//校验位
						ZigBee_data_send[11] = 0x0d;
						ZigBee_data_send[12] = 0x0a;
						printf("%s",ZigBee_data_send);	//发送控制命令
						relay2_flag=0;					//关闭状态
					}
					break;
					
				case 0x03:	//MQ2，控制蜂鸣器报警
					if(data[5]>=MQ2_MAX&&buzzer_flag==0)//超出阈值,且蜂鸣器是关则的，开
					{
						memset(ZigBee_data_send,0,16);
						ZigBee_data_send[0] = 0xa5;
						ZigBee_data_send[1] = 0x5a;
						ZigBee_data_send[2] = 0x03;		//协调器
						ZigBee_data_send[3] = 0x01;		//控制
						ZigBee_data_send[4] = 0x04;		//蜂鸣器
						ZigBee_data_send[5] = 0x01;		//开
						ZigBee_data_send[6] = 0xff;		//
						ZigBee_data_send[7] = 0xff;
						ZigBee_data_send[8] = 0xff;
						ZigBee_data_send[9] = 0x0a;		//长度
						ZigBee_data_send[10] = 0xff;	//校验位
						ZigBee_data_send[11] = 0x0d;
						ZigBee_data_send[12] = 0x0a;
						printf("%s",ZigBee_data_send);	//发送控制命令
						buzzer_flag=1;					//1代表打开状态
					}
					else if(data[5]<=MQ2_MAX&&buzzer_flag==1)//低于阈值,且蜂鸣器是开则的，关
					{
						memset(ZigBee_data_send,0,16);
						ZigBee_data_send[0] = 0xa5;
						ZigBee_data_send[1] = 0x5a;
						ZigBee_data_send[2] = 0x03;		//协调器
						ZigBee_data_send[3] = 0x01;		//控制
						ZigBee_data_send[4] = 0x04;		//蜂鸣器
						ZigBee_data_send[5] = 0x02;		//关
						ZigBee_data_send[6] = 0xff;		//
						ZigBee_data_send[7] = 0xff;
						ZigBee_data_send[8] = 0xff;
						ZigBee_data_send[9] = 0x0a;		//长度
						ZigBee_data_send[10] = 0xff;	//校验位
						ZigBee_data_send[11] = 0x0d;
						ZigBee_data_send[12] = 0x0a;
						printf("%s",ZigBee_data_send);	//发送控制命令
						buzzer_flag=0;					//关闭状态
					}					
					break;
					
				case 0x04:	//光敏，控制协调器上的LED2,相当于开灯
					if(data[5]>=LIGHT_MAX&&led_flag==1)	//超出阈值，且灯开着，关
					{
						memset(ZigBee_data_send,0,16);
						ZigBee_data_send[0] = 0xa5;
						ZigBee_data_send[1] = 0x5a;
						ZigBee_data_send[2] = 0x03;		//协调器
						ZigBee_data_send[3] = 0x01;		//控制
						ZigBee_data_send[4] = 0x05;		//led
						ZigBee_data_send[5] = 0x02;		//关闭
						ZigBee_data_send[6] = 0xff;		//补充
						ZigBee_data_send[7] = 0xff;
						ZigBee_data_send[8] = 0xff;
						ZigBee_data_send[9] = 0x0a;		//长度
						ZigBee_data_send[10] = 0xff;	//校验位
						ZigBee_data_send[11] = 0x0d;
						ZigBee_data_send[12] = 0x0a;
						printf("%s",ZigBee_data_send);	//发送控制命令
						led_flag = 0;				//0表示关闭状态
					}
					else if(data[5]<=LIGHT_MAX&&led_flag==0)	//低于阈值，且灯关着，开
					{
						memset(ZigBee_data_send,0,16);
						ZigBee_data_send[0] = 0xa5;
						ZigBee_data_send[1] = 0x5a;
						ZigBee_data_send[2] = 0x03;		//协调器
						ZigBee_data_send[3] = 0x01;		//控制
						ZigBee_data_send[4] = 0x05;		//led
						ZigBee_data_send[5] = 0x01;		//打开
						ZigBee_data_send[6] = 0xff;		//补充
						ZigBee_data_send[7] = 0xff;
						ZigBee_data_send[8] = 0xff;
						ZigBee_data_send[9] = 0x0a;		//长度
						ZigBee_data_send[10] = 0xff;	//校验位
						ZigBee_data_send[11] = 0x0d;
						ZigBee_data_send[12] = 0x0a;
						printf("%s",ZigBee_data_send);	//发送控制命令
						led_flag = 1;				//打开状态
					}
					break;
					
				default:
					break;
				}
		}
	}
}

void device_state(u8 data[])
{
	if(data[0]==0xa5&&data[1]==0x5a)	//帧头判断
	{
		if(data[3] == 0x03)				//设备状态
		{
			switch (data[4])
			{
				case 0x01:				//继电器,空调
					if(data[2]==0x02)	//节点二
					{
						if(data[5]==0x01)
							relay2_flag = 1;	//开
						else if(data[5]==0x02)
							relay2_flag = 2;	//关
					}
					break;
					
				case 0x03:				//步进电机
					if(data[5]==0x01)
						motor_flag = 1;	//开
					else if(data[5]==0x02)
						motor_flag = 2;	//关
					break;
					
				case 0x04:				//蜂鸣器
					if(data[5]==0x01)
						buzzer_flag = 1;	//开
					else if(data[5]==0x02)
						buzzer_flag = 2;	//关
					break;
					
				case 0x05:				//led灯
					if(data[5]==0x01)
						led_flag = 1;	//开
					else if(data[5]==0x02)
						led_flag = 2;	//关
					break;
				break;
				default:
					break;
			}
		}
	}
}
